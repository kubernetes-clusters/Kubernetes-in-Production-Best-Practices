terraform {
  backend "s3" {
    bucket         = "thingsboard-vpc-terraform-state"
    key            = "thingsboard-vpc.tfstate"
    region         = "us-east-1"
    dynamodb_table = "thingsboard-vpc-terraform-state-lock-dynamodb"
  }
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.31.0"
    }
  }
  required_version = "~> 1.0.0"
}
